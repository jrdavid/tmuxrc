cwd = $(shell pwd)

install:
	ln -sf $(cwd) $(HOME)/.tmux
	ln -sf $(cwd)/tmux.conf $(HOME)/.tmux.conf
	mkdir -p -- ~/var/tmux
